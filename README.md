# Fotowall

Sources: https://github.com/enricoros/fotowall

Desktop APP that lets you create graphical compositions
 Fotowall is a desktop APP that lets you create graphical compositions by
 layering and manipulating photos and pictures, text, live video, wordclouds,
 and drag&drop content from the internet.  All to create a fun graphical
 composition with the maximum ease of use!